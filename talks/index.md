---
layout: page
title: Here you can find all my presentation slides/videos and also know where you can find me in the future.
tags: [about, Jekyll, theme, responsive]
modified: 2015-09-27T20:53:07.573882-04:00
comments: true
image:
  feature: background.jpg
  credit: unsplash
  creditlink: https://unsplash.com/
---

* Will be replaced with the ToC, excluding the "Contents" header
{:toc}


## Talk 1

#### Subject 1

- **Title:** [Link](https://truongdam.github.io)
- **Slides:** [Slides](https://truongdam.github.io)

- **Title:** [Link](https://truongdam.github.io)
- **Slides:** [Slides](https://truongdam.github.io)

#### Subject 2

- **Title:** [Link](https://truongdam.github.io)
- **Slides:** [Slides](https://truongdam.github.io)

<hr>

## Talk 2

#### Subject 3

- **Title:** [Link](https://truongdam.github.io)
- **Slides:** [Slides](https://truongdam.github.io)

- **Title:** [Link](https://truongdam.github.io)
- **Slides:** [Slides](https://truongdam.github.io)

#### Subject 4

- **Title:** [Link](https://truongdam.github.io)
- **Slides:** [Slides](https://truongdam.github.io)
