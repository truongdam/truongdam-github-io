an interceptor is a way to modify any incoming / outgoing http request. If you’ve worked with Express.Js, it’s kind of like middleware.

Interceptors are used for authenticating web requests, modifying request headers, sending redirects, logging events, etc.