#### Create hard links in Linux
```
ls -l
ln <source> link
ln topprocs.sh tp
ls -l
```

#### Change hard links into soft links
```
ln -P topprocs.sh tp

```
#### Create Symbolic links in Linux
```
ln -s -bin/topprocs.sh tops.sh
ls -l
```

#### ssh
```
ssh user@serverip
```

#### Find a file
```
find *.html
```