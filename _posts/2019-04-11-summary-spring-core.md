Spring Core

##### Spring Container
* BeanFactory Container
* AplicationContext Container
Difference: [link](https://www.oodlestechnologies.com/blogs/Difference-between-BeanFactory-and-ApplicationContext)

```
lazy load >< eager load
not supports >< supports internationalization
explicitly provided a resource object >< creates and manages resources objects
not supported @PreDestroy, @Autowired >< supported annotation
```

##### Bean Scopes
* singleton (default*), prototype, request, session, global-session

##### Bean Life Cycle
* Creation of bean instance by a factory method
* Set the values and bean references to bean properties
* Call the initialization callback method
* Bean is ready for user
* Call the destruction callback method

link: https://www.javainterviewpoint.com/spring-bean-life-cycle/

3 Ways:

```
Implement InitializingBean and DisposableBean and overiding afterPropertiesSet() and destroy()
init-method, destroy-method
@PostConstruct, @PreDestroy

```

##### BeanPostProcessor
link: https://howtodoinjava.com/spring-core/how-to-create-spring-bean-post-processors/

Call before and after init bean for all bean instances.
```
Implement BeanPostProcessor

then implement postProcessBeforeInitialization() and postProcessAfterInitialization() methods
```

##### Spring Bean Autowiring
* no, byType (default*), byName, constructor, autodetect (constructor -> byType)
* Using @Autowired on:
```
    properties, property setters, constructors
```

##### Resource Loader
link: https://howtodoinjava.com/spring-core/how-to-load-external-resources-files-into-spring-context/
```
implement either the ApplicationContextAware interface or the ResourceLoaderAware interface
```

##### Final Static beans
link: https://howtodoinjava.com/spring-core/spring-declare-beans-from-final-static-field-references-using-util-constant/
```
using <util:constant />
```

##### Static factory, Factory Bean (create bean instances)

@Configuration

